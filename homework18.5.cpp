﻿
#include <iostream>




class Stack
{

private:

    int x;
    int memory;
    int* p;

public:
    //Конструктор, создающий массив
    Stack () : x(0), memory(1)
    {
        p = new int[memory];
        
        std::cout << "\n";
    }

    //Метод, выводящий на экран элементы массива
    void show()
    {
        for (int i = 0; i < x; ++i)
        {
            std::cout << p[i];
        }

        std::cout << "\n";
    }

    //Метод, убиращий последний элемент массива
    void pop()
    {
        std::cout << "\nУбираем последний элемент массива\n";

        if (x <= 0)
        {
            x = 0;
            std::cout << "В массиве нет элементов, которые можно было бы убрать\n";
        }
        else
        {
            x -= 1;
        }

        show();
    }

    //Метод, добавляющий новый элемент массива
    void push()
    {
        
        //Если памяти выделено недостачно, выделяем вдвое больше необходимой
        if (memory <= x)
        {
            memory = x * 2;
            int* p2 = new int[memory];

            for (int i = 0; i < memory; ++i)
            {

                p2[i] = p[i];

            }

            p = p2;
        }

        std::cout << "\nВведите новый элемент массива ";
        std::cin >> p[x];

        x += 1;

        show();
    }

    ~Stack()
    {
        delete[] p;
    }

};

int main()
{
    setlocale(LC_ALL, "Russian");

    Stack test;

    test.pop();

    test.push();
    test.push();
    test.push();

    test.pop();
    test.pop();
    test.pop();
    test.pop();
}